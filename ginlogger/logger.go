package ginlogger

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"gitlab.com/bonch.dev/go-lib/gin-sub/ginbodywriter"
)

var (
	MaxRequestLengthForLog  int64 = 250
	MaxResponseLengthForLog int64 = 1000
	IgnoreClosedHttp        bool  = false
)

var timeFormat = "02/Jan/2006:15:04:05 -0700"

// Logger is the logrus logger handler
func Logger(logger logrus.FieldLogger) gin.HandlerFunc {
	hostname := getHostname()

	maxRequestLength := MaxRequestLengthForLog
	maxResponseLength := MaxResponseLengthForLog
	errorStatusCodes := ErrorStatusCodes
	warnStatusCodes := WarnStatusCodes
	ignoreClosedHttp := IgnoreClosedHttp

	return func(ctx *gin.Context) {
		blw := ginbodywriter.NewWriter(ctx.Writer)
		ctx.Writer = blw

		path, method, referrer, userAgent := getContextData(ctx)
		if isExcludePath(path) {
			return
		}

		var requestBody string
		if ctx.Request.ContentLength < maxRequestLength {
			ctx.Request.Body, requestBody = copyBody(ctx.Request.Body)
		} else {
			requestBody = "HIDDEN DATA: too heavy"
		}

		latency := calculateLatency(ctx.Next)

		var responseBody string
		responseBody = blw.Body.String()
		if len(responseBody) > int(maxResponseLength) {
			responseBody = "HIDDEN DATA: too heavy"
		}

		statusCode := ctx.Writer.Status()

		dataLength := ctx.Writer.Size()
		if dataLength < 0 {
			dataLength = 0
		}

		entry := logger.WithFields(logrus.Fields{
			"gin":            true,
			"hostname":       hostname,
			"statusCode":     statusCode,
			"latency":        latency, // time to process
			"clientIP":       ctx.ClientIP(),
			"method":         method,
			"path":           path,
			"referer":        referrer,
			"dataLength":     dataLength,
			"userAgent":      userAgent,
			"requestBody":    requestBody,
			"requestHeaders": ctx.Request.Header,
			"responseBody":   responseBody,
			"controller":     ctx.HandlerName(),
		})

		entry = entry.WithContext(ctx)

		if ignoreClosedHttp && errors.Is(ctx.Err(), context.Canceled) {
			return
		}

		if len(ctx.Errors) > 0 {
			entry.Error(ctx.Errors.ByType(gin.ErrorTypePrivate).String())
		} else {
			msg := fmt.Sprintf("[%s] \"%s %s\" %d",
				time.Now().Format(timeFormat),
				ctx.Request.Method,
				path,
				statusCode,
			)
			switch {
			case errorStatusCodes(statusCode):
				entry.Error(msg)
			case warnStatusCodes(statusCode):
				entry.WithField("skipSentry", "true").Warn(msg)
			default:
				entry.WithField("skipSentry", "true").Info(msg)
			}
		}
	}
}

func calculateLatency(f func()) int64 {
	start := time.Now()

	f()

	return time.Since(start).Milliseconds()
}

func getHostname() string {
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}

	return hostname
}

//nolint:gocritic
func getContextData(c *gin.Context) (string, string, string, string) {
	return c.Request.URL.Path,
		c.Request.Method,
		c.Request.Referer(),
		c.Request.UserAgent()
}

//nolint:gocritic
func copyBody(reader io.Reader) (io.ReadCloser, string) {
	bodyBytes, err := ioutil.ReadAll(reader)
	if err != nil {
		return ioutil.NopCloser(bytes.NewBufferString("")), ""
	}

	return ioutil.NopCloser(bytes.NewBuffer(bodyBytes)), string(bodyBytes)
}
