package ginlogger

import "net/http"

type StatusCodesParser func(statusCode int) bool

var (
	ErrorStatusCodes StatusCodesParser = func(statusCode int) bool {
		return statusCode >= http.StatusInternalServerError
	}
	WarnStatusCodes StatusCodesParser = func(statusCode int) bool {
		return statusCode >= http.StatusMultipleChoices &&
			statusCode < http.StatusInternalServerError
	}
	InfoStatusCodes StatusCodesParser = func(statusCode int) bool {
		return statusCode >= http.StatusOK && statusCode < http.StatusMultipleChoices
	}
)
