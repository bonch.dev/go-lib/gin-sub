package ginstracer

import "strings"

var ExcludePath = []string{"__debug", "metrics", "__helper"}

func AddExcludePath(path string) {
	ExcludePath = append(ExcludePath, path)
}

func isExcludePath(path string) bool {
	for _, exclude := range ExcludePath {
		if strings.Contains(path, exclude) {
			return true
		}
	}

	return false
}
