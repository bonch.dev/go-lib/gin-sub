package ginstracer

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"

	"gitlab.com/bonch.dev/go-lib/gin-sub/ginalias"
)

func Tracer() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fullPathName := ctx.FullPath()

		if isExcludePath(fullPathName) ||
			isExcludePath(ctx.Request.URL.Path) ||
			ginalias.IsAliased(ctx) {
			ctx.Next()
			return
		}

		span := sentry.StartTransaction(
			ctx.Request.Context(),
			fmt.Sprintf("%s %s", ctx.Request.Method, fullPathName),
			sentry.WithOpName("http.route"),
			sentry.ContinueFromTrace(ctx.Request.Header.Get(sentry.SentryTraceHeader)),
			sentry.WithTransactionSource(sentry.SourceRoute),
		)
		defer span.Finish()

		span.SetTag("URL", fullPathName)
		span.SetTag("RealURL", ctx.Request.URL.Path)
		span.SetTag("Controller", ctx.HandlerName())
		span.SetTag("Method", ctx.Request.Method)

		ctx.Request = ctx.Request.WithContext(span.Context())

		// add request as additional information to current hub in context
		if hub := sentry.GetHubFromContext(ctx.Request.Context()); hub != nil {
			hub.Scope().SetRequest(ctx.Request)
		}

		ctx.Next()

		if errors.Is(ctx.Err(), context.Canceled) {
			span.Status = sentry.SpanStatusCanceled
		} else {
			span.Status = collectSpanStatus(ctx.Writer.Status())
		}

		span.SetTag("http.status_code", strconv.Itoa(ctx.Writer.Status()))
	}
}

func collectSpanStatus(status int) sentry.SpanStatus {
	switch status {
	case http.StatusOK, http.StatusCreated:
		return sentry.SpanStatusOK
	case http.StatusBadRequest, http.StatusUnprocessableEntity:
		return sentry.SpanStatusInvalidArgument
	case http.StatusConflict:
		return sentry.SpanStatusFailedPrecondition
	case http.StatusFailedDependency, http.StatusInternalServerError:
		return sentry.SpanStatusInternalError
	case http.StatusUnauthorized:
		return sentry.SpanStatusUnauthenticated
	case http.StatusForbidden:
		return sentry.SpanStatusPermissionDenied
	case http.StatusNotFound:
		return sentry.SpanStatusNotFound
	}

	if status >= http.StatusInternalServerError {
		return sentry.SpanStatusInternalError
	}

	return sentry.SpanStatusUnknown
}
