package ginalias

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

var AliasKey = "aliased"

type aliasMap map[string]string

func (m aliasMap) AddAlias(from, to string) {
	m[from] = to
}

func (m aliasMap) GetAlias(from string) (string, bool) {
	for f := range m {
		if strings.HasPrefix(from, f) {
			return m[f] + strings.TrimPrefix(from, f), true
		}
	}

	return "", false
}

var Aliases = aliasMap{}

func Aliaser(e http.Handler) gin.HandlerFunc {
	return func(c *gin.Context) {
		alias, ok := Aliases.GetAlias(c.Request.URL.Path)
		if ok {
			c.Request.URL.Path = alias
			c.Set(AliasKey, true)
			e.ServeHTTP(c.Writer, c.Request)

			c.Abort()
		}

		c.Next()
	}
}

func IsAliased(c *gin.Context) bool {
	return c.GetBool(AliasKey)
}
