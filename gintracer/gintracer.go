package gintracer

import (
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"

	"gitlab.com/bonch.dev/go-lib/jtracer"
)

var GinJaegerSpanKey = "JaegerSpan"

func Tracer(t opentracing.Tracer) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var (
			span       opentracing.Span
			tracerType = jtracer.Type{T: "", N: ctx.FullPath()}
			carrier    = opentracing.HTTPHeadersCarrier(ctx.Request.Header)
		)

		spanContext, err := t.Extract(opentracing.HTTPHeaders, carrier)
		if err == nil {
			span = jtracer.FromParentContextWithTracer(tracerType, spanContext, t)
		} else {
			span = jtracer.FromContextWithTracer(ctx, jtracer.Type{T: "", N: ctx.FullPath()}, t)
		}

		span.SetTag("URL", ctx.FullPath())
		span.SetTag("RealURL", ctx.Request.URL.Path)
		span.SetTag("Controller", ctx.HandlerName())

		ctx.Set(GinJaegerSpanKey, span)

		ctx.Next()

		span.Finish()
	}
}
