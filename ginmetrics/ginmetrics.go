package ginmetrics

import (
	"os"

	"github.com/Depado/ginprom"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
)

// BucketSize - size of buckets for bucketer metrics (http response time) - in seconds
var BucketSize = []float64{
	.025,
	.05,
	.1,
	.25,
	.5,
	.75,
	1,
	1.25,
	1.5,
	2,
	2.5,
	5,
}

func Init(g *gin.Engine) gin.HandlerFunc {
	p := ginprom.New(
		ginprom.BucketSize(BucketSize),
		ginprom.Engine(g),
		ginprom.Subsystem(""),
		ginprom.Path("/metrics"),
		ginprom.Ignore("/__helper/status"),
		promTagApplicationVersion,
	)

	return p.Instrument()
}

func promTagApplicationVersion(_ *ginprom.Prometheus) {
	tag, exists := os.LookupEnv("CI_APPLICATION_TAG")
	if !exists {
		tag = "local"
	}

	g := prometheus.NewCounter(prometheus.CounterOpts{
		Name:        "application_info",
		Help:        "Core Application version",
		ConstLabels: map[string]string{"version": tag},
	})
	g.Add(1)

	prometheus.MustRegister(g)
}
