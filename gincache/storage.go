package gincache

import (
	"context"
	"time"
)

type Storager interface {
	Get(ctx context.Context, key string) ([]byte, error)
	Set(ctx context.Context, key string, content []byte, duration time.Duration) error
}
