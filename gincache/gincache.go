package gincache

import (
	"encoding/json"

	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go/log"
	"github.com/sirupsen/logrus"
	"gitlab.com/bonch.dev/go-lib/jtracer"

	"gitlab.com/bonch.dev/go-lib/gin-sub/ginbodywriter"
)

func CacheResponse(logger *logrus.Logger, storage Storager) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx, span := jtracer.WithGinContext(ctx, jtracer.WithType("Middleware"))
		defer span.Finish()

		gbw := ginbodywriter.NewWriter(ctx.Writer)
		ctx.Writer = gbw

		cacheBytes, err := storage.Get(ctx, "route_cache_"+ctx.Request.RequestURI)
		if err == nil {
			var cache Cache

			if err := json.Unmarshal(cacheBytes, &cache); err != nil {
				logger.WithError(err).Error("error when unmarshaling cached data, skip cache.")

				goto NoCacheBehavior
			}

			span.LogFields(log.Bool("Cache hit", true))

			ctx.Header("XCached", "true")
			ctx.Data(cache.Status, "application/json", cache.Body)
			ctx.Abort()

			return
		}

	NoCacheBehavior:
		span.LogFields(log.Bool("Cache hit", false))

		ctx.Next()

		cacheSettings, set := ctx.Get("cache")
		if !set {
			return
		}

		setting, ok := cacheSettings.(Setting)
		if !ok {
			logger.Error("wrong settings given")

			return
		}

		cacheBytes, err = Cache{
			Status: ctx.Writer.Status(),
			Body:   gbw.Bytes(),
		}.ToBytes()

		if err != nil {
			logger.WithError(err).Error("error when marshaling data for caching, skip cache.")

			return
		}

		storage.Set(
			ctx,
			"route_cache_"+ctx.Request.RequestURI,
			cacheBytes,
			setting.CacheDuration,
		)
	}
}
